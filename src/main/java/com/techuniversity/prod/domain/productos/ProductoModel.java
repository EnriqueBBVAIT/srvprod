package com.techuniversity.prod.domain.productos;

import lombok.*;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "productos")
public class ProductoModel {

    @Id
    @NotNull
    private String id;
    private String nombre;
    private String descripcion;
    private Double precio;


}
